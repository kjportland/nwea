import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;

public class War
{

    private static final Logger log= LoggerFactory.getLogger(War.class);

    public void play( int numberOfSuits, int numberOfRanks, int numberOfPlayers ) throws Exception {

        if(numberOfPlayers>4){
            System.out.println("This game only supports less than 4 players");
            return;
        }

        //divide evenly.
        int totalCards = numberOfSuits*numberOfRanks;
        totalCards = totalCards - ( totalCards % numberOfPlayers);
        int noOfCardsForEachPlayer = totalCards/numberOfPlayers;

        //lets deal the cards
        Dealer dealer = new Dealer();
        dealer.create(numberOfSuits,numberOfRanks);
        dealer.shuffle();

        Players  players = new Players().createPlayers(numberOfPlayers,noOfCardsForEachPlayer,dealer);
        int roundCounter=0;

        while (players.size()>1){
            log.info("After round={} the players card={}",roundCounter++,players.toString());
            round(players, null,false);
        }

        log.info("The winner of the game is player={}",players.toString());

    }

    private void round(Players players,Queue<Card> warCards ,boolean warRound){

        if(players.size()==1)
            return;
        if(!players.isAtWarAndFindWinner(players)){

           players.addCardsToRoundWinner(players, warCards,warRound);

        }else {
            warCards= players.addCardsToPileForWar(warCards,players);
            log.info("player is at war");
            round(players,warCards, true);
        }
    }

    public static void main( String[] args) throws Exception {

   //     logger.debug("Log4j appender configuration is successful !!");
        if(args.length>0){
            War war = new War();
            war.play(Integer.valueOf(args[0]),Integer.valueOf(args[1]),Integer.valueOf(args[2]));
        }else {
            log.info("Please pass the arguments with separated by spaces( Suits, Rank and players");
        }

    }
}
