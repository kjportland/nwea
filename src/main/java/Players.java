import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Getter @Setter
public class Players extends ArrayList<Player> {

    private static final Logger log = LoggerFactory.getLogger(Players.class);

    public Players createPlayers(int noOfPlayers, int noOfCardsForEachPlayer, Dealer dealer) {

        // assigning payers cards to play.
        Players myPlayers = new Players();
        for (int i = 0; i < noOfPlayers; i++) {
            int counter = 0;
            Queue<Card> eachPlayersDeck = new LinkedList<Card>();
            while (counter++ < noOfCardsForEachPlayer) {
                Card card = dealer.deal();
                eachPlayersDeck.add(card);
            }
            Player player = Player.builder().player(i).cards(eachPlayersDeck).build();
            //    log.info("player={}", player);
            myPlayers.add(player);
        }

        return myPlayers;
    }

    public boolean isAtWarAndFindWinner(Players players) {

        if (players.size() == 1) {
            return false;
        }

        int higher = 0;
        boolean isAtWar = false;

        for(int i=0;i<players.size();i++){
            Player p = players.get(i);
            int cardOrdinal = p.getCards().peek().getOrdinal();
            if (higher == cardOrdinal) {
                isAtWar = true;
                p.setRoundWinner(false);
            } else if (cardOrdinal > higher) {
                isAtWar = false;
                p.setRoundWinner(true);
                if (i>0){
                    players.get(i-i).setRoundWinner(false);
                }
                higher = cardOrdinal;
            }


        }
        return isAtWar;
    }





    public void addCardsToRoundWinner(Players players, Queue<Card> warCards ,boolean warRound) {

        ArrayList<Integer> indexes = new ArrayList<>();
        int playersIndex = 0;
        Optional<Player> roundWinner = players.stream().filter(player -> player.isRoundWinner()).findFirst();

        if (!roundWinner.isPresent()) {
            log.error("there are no winners this time");
        }

        if(warRound){
            for(Card c : warCards){
                roundWinner.get().getCards().add(c);
            }
        }

        for (Player p : players) {
            roundWinner.get().getCards().add(p.getCards().remove());
            if(p.getCards().size()==0) {
                log.info( "Player={} is out of the game",p.getPlayer());
                indexes.add(playersIndex);
            }
            p.setRoundWinner(false);
            playersIndex++;
        }
        for(int i :indexes){
            log.info("PlayerIndex={}",  i);
            players.remove(i);
        }

    }

    public Queue<Card> addCardsToPileForWar(Queue<Card> cards, Players players) {

        ArrayList<Integer> indexes = new ArrayList<>();

        if(cards==null){
            cards = new LinkedList<>();
        }
        int playersIndex = 0;
        for (Player p : players) {
            cards.add(p.getCards().remove());
            if(p.getCards().size()==0) {
                log.info( "Player={} is out of the game",p.getPlayer());
                indexes.add(playersIndex);
            }
            p.setRoundWinner(false);
            playersIndex++;
        }

        for(int i :indexes){
            log.info("PlayerIndex={}",  i);
            players.remove(i);
        }
        return cards;

    }

}
