public interface Deck{

    /* Create the deck of cards */
    public void create( int numberOfSuits, int numberOfRanks ) throws Exception;


    /* Shuffle the deck */
    public void shuffle();


    /* deal a card from the deck */
    public Card deal();
}
