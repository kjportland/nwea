import lombok.*;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.UUID;


@Builder
@AllArgsConstructor
@ToString
@Getter
@Setter
@EqualsAndHashCode
public class Player {

    public int player;
    public Queue<Card> cards ;

    public boolean isRoundWinner;


}
