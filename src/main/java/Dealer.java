import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

@Getter
@NoArgsConstructor
public class Dealer implements Deck {

   public Stack<Card> deckOfCards = new Stack<Card>();


    public void create(int numberOfSuits, int numberOfRanks) throws Exception {

        if(numberOfSuits>4 || numberOfRanks>14){
            throw new Exception("Invalid no of suits or cards");
        }

        for(int i=0;i<numberOfSuits; i++){
            for (int j=0;j<numberOfRanks;j++){
                deckOfCards.push(Card.builder().suit(Suit.values()[i]).rank(Rank.values()[j]).build());
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(this.getDeckOfCards());
    }

    public Card deal() {
        return deckOfCards.pop();
    }

}
