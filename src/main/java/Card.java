import lombok.*;

@Builder
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Card {

    @EqualsAndHashCode.Exclude
    public Suit suit;

    @EqualsAndHashCode.Exclude
    public Rank rank;

    @Getter(lazy = true)
    @EqualsAndHashCode.Include
    @ToString.Exclude
    private final int ordinal = rank.ordinal() + 2; // +2 is to match ordinal.


}



