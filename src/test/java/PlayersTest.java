import org.junit.Test;

import java.util.LinkedList;
import java.util.Queue;

import static org.junit.Assert.*;

public class PlayersTest {




    @Test
    public void testIsAtWarAndFindRoundWinnerFirstPlayer(){

          Queue<Card> player1Cards = new LinkedList<>();
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Four).build());
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Five).build());

        Queue<Card> player2Cards = new LinkedList<>();
        player2Cards.add(Card.builder().suit(Suit.Spades).rank(Rank.Nine).build());
        player2Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Two).build());

        Player player1 =  Player.builder().player(1).cards(player1Cards).build();
        Player player2 =  Player.builder().player(2).cards(player2Cards).build();

        Players players = new Players();
        players.add(player1);
        players.add(player2);

        players.isAtWarAndFindWinner(players);
        assertTrue ( players.get(1).isRoundWinner());
        assertFalse(players.get(0).isRoundWinner());

    }

    @Test
    public void AddCardsToRoundWinnerFirstPlayer(){

        Queue<Card> player1Cards = new LinkedList<>();
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Four).build());
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Five).build());

        Queue<Card> player2Cards = new LinkedList<>();
        player2Cards.add(Card.builder().suit(Suit.Spades).rank(Rank.Nine).build());
        player2Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Two).build());

        Player player1 =  Player.builder().player(1).cards(player1Cards).build();
        Player player2 =  Player.builder().player(2).cards(player2Cards).build();

        Players players = new Players();
        players.add(player1);
        players.add(player2);

        players.isAtWarAndFindWinner(players);
        assertTrue ( players.get(1).isRoundWinner());
        assertFalse(players.get(0).isRoundWinner());

        players.addCardsToRoundWinner(players,null,false);

        assertEquals(3,player2.getCards().size());
        assertEquals(1,player1.getCards().size());
        assertFalse(player1.isRoundWinner());
        assertFalse(player2.isRoundWinner());

    }

    @Test
    public void TestFirstdPlayerOutofTheGame(){
        Queue<Card> player1Cards = new LinkedList<>();
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Two).build());


        Queue<Card> player2Cards = new LinkedList<>();
        player2Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Three).build());
        player2Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Five).build());
        player2Cards.add(Card.builder().suit(Suit.Spades).rank(Rank.Nine).build());

        Player player1 =  Player.builder().player(1).cards(player1Cards).build();
        Player player2 =  Player.builder().player(2).cards(player2Cards).build();

        Players players = new Players();
        players.add(player1);
        players.add(player2);

        players.isAtWarAndFindWinner(players);
        assertTrue ( players.get(1).isRoundWinner());
        assertFalse(players.get(0).isRoundWinner());

        players.addCardsToRoundWinner(players,null,false);

        assertEquals(4,player2.getCards().size());
        assertEquals(1,players.size());
        assertFalse(player1.isRoundWinner());

    }

    @Test
    public void TestFirstdPlayerOutofTheGameWithWar(){
        Queue<Card> player1Cards = new LinkedList<>();
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Two).build());


        Queue<Card> player2Cards = new LinkedList<>();
        player2Cards.add(Card.builder().suit(Suit.Spades).rank(Rank.Two).build());
        player2Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Five).build());
        player2Cards.add(Card.builder().suit(Suit.Spades).rank(Rank.Nine).build());

        Player player1 =  Player.builder().player(1).cards(player1Cards).build();
        Player player2 =  Player.builder().player(2).cards(player2Cards).build();

        Players players = new Players();
        players.add(player1);
        players.add(player2);

        boolean isAtWar = players.isAtWarAndFindWinner(players);
        assertTrue ( isAtWar);

        Queue<Card> cards = players.addCardsToPileForWar(null,players);

        assertEquals(2,player2.getCards().size());
        assertEquals(2,cards.size());
        assertEquals(1,players.size());
        assertFalse(player1.isRoundWinner());

    }


    @Test
    public void testIsAtWarAndFindRoundWinnerSecondPlayer(){

        Queue<Card> player1Cards = new LinkedList<>();
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Ace).build());
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Five).build());

        Queue<Card> player2Cards = new LinkedList<>();
        player2Cards.add(Card.builder().suit(Suit.Spades).rank(Rank.Nine).build());
        player2Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Two).build());

        Player player1 =  Player.builder().player(1).cards(player1Cards).build();
        Player player2 =  Player.builder().player(2).cards(player2Cards).build();

        Players players = new Players();
        players.add(player1);
        players.add(player2);

        players.isAtWarAndFindWinner(players);
        assertFalse ( players.get(1).isRoundWinner());
        assertTrue ( players.get(0).isRoundWinner());

    }


    @Test
    public void AddCardsToRoundWinnerSecondPlayer(){

        Queue<Card> player1Cards = new LinkedList<>();
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Ace).build());
        player1Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Five).build());

        Queue<Card> player2Cards = new LinkedList<>();
        player2Cards.add(Card.builder().suit(Suit.Spades).rank(Rank.Nine).build());
        player2Cards.add(Card.builder().suit(Suit.Hearts).rank(Rank.Two).build());

        Player player1 =  Player.builder().player(1).cards(player1Cards).build();
        Player player2 =  Player.builder().player(2).cards(player2Cards).build();

        Players players = new Players();
        players.add(player1);
        players.add(player2);

        players.isAtWarAndFindWinner(players);
        assertFalse ( players.get(1).isRoundWinner());
        assertTrue ( players.get(0).isRoundWinner());

        players.addCardsToRoundWinner(players,null,false);

        assertEquals(1,player2.getCards().size());
        assertEquals(3,player1.getCards().size());
        assertFalse(player1.isRoundWinner());
        assertFalse(player2.isRoundWinner());

    }



}
