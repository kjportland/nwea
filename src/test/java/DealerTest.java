import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DealerTest {

    @Test
    public void TestDeckCreate() throws Exception {
        int numberOfSuits = 2;
        int numberOfRanks = 5;
        Dealer dealer = new Dealer();
        dealer.create(numberOfSuits,numberOfRanks);
        assertEquals(2*5,dealer.getDeckOfCards().size());
    }

    @Test
    public void ShuffleCards() throws Exception {
        int numberOfSuits = 4;
        int numberOfRanks = 12;
        Dealer dealer = new Dealer();
        dealer.create(numberOfSuits,numberOfRanks);
        Card a = dealer.getDeckOfCards().get(1);
        dealer.shuffle();
        Card aAfterSuffle = dealer.getDeckOfCards().get(1);
        assertNotEquals(a,aAfterSuffle);
    }
}
